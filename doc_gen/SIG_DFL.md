```
# define SIG_DFL ((sighandler_t) 0)
```

If set on a process for a signal, the action to be performed by this signal is the default action.
