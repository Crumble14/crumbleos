```
# define ABS(i) ((i) < 0 ? -(i) : (i))
```

Returns the absolute value of the given **i**.
