```
void cpu_reset(void);
```

Triggers a CPU reset, which causes the computer to reboot.
