```
void outl(uint16_t port, uint32_t value);
```

Writes four bytes to the specified port.
