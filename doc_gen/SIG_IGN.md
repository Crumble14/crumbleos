```
# define SIG_IGN ((sighandler_t) 1)
```

If set on a process for a signal, the signal is ignored by the process (unless the signal cannot be ignored).
