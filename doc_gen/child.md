```
struct child
```

Structure used to make a linked list of children processes.

- **next**: The next child
- **process**: The process
