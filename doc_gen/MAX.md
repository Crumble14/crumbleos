```
# define MAX(a, b) ((a) >= (b) ? (a) : (b))
```

Returns the greatest value between **a** and **b**.
