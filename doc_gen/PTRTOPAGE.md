```
# define PTRTOPAGE(ptr) ((uintptr_t) (ptr) / PAGE_SIZE)
```

Gives the page number for the specified **ptr**.
