```
uint8_t inb(uint16_t port);
```

Reads one byte from the specified port.
