```
void vga_putchar_color(char c, uint8_t color, vgapos_t x, vgapos_t y);
```

In VGA text mode, writes the specified character **c** with the specified **color** at the specified position.
