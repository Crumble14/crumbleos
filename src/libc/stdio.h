#ifndef _STDIO_H
# define _STDIO_H

# include <stdarg.h>

__attribute__((format(printf, 1, 2)))
int printf(const char *format, ...);

#endif
