#ifndef _STDLIB_H
# define _STDLIB_H

# include <libc/string.h>

int atoi(const char *nptr);
long atol(const char *nptr);

#endif
